<div class="col-lg-3">

    <!-- Shop Sidebar -->
    <div class="shop_sidebar">
        <div class="sidebar_section">
            <ul class="sidebar_categories">
                <li class="active"><a href="{{ route('myAccount.index') }}">My Account</a></li>
                <li><a href="{{ route('orders.index') }}">My Orders</a></li>
            </ul>
        </div>
    </div>

</div>
