<?php

namespace Product\Repositories;

use Illuminate\Support\Arr;
use Core\Eloquent\BaseRepository;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Storage;
use Illuminate\Container\Container as App;


class ProductRepository extends BaseRepository
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(App $app)
    {
        parent::__construct($app);
    }
    /**
     * Specify Model class name
     *
     * @return mixed
     */
    public function model()
    {
        return 'Product\Models\Product';
    }
    /**
     * Create a new product record in the database with thumb.
     *
     * @param array $data
     *
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function create(array $data)
    {
        $product = $this->model->create($data);
        $variations = json_decode($data['variations'], true);

        if($data['type'] == 'configurable'){
            $product->attributes()->attach($data['attributes']);
            foreach ($variations as $variant) {
                $this->createVariant($variant, $product, $data);
            }
        }

        return $product;
    }
    /**
     * @param mixed $product
     * @param array $permutation
     * @param array $data
     * @return mixed
     */
    public function createVariant($variant ,$product, $data)
    {
        $attributes_ids = Arr::pluck($variant['attributes'], 'id');

        $data = [
            "sku" => $product->sku . '-variant-' . implode('-', $attributes_ids),
            "name" => "",
            "price" => $variant['price'] ?? $data['price'],
            "cost" => $variant['cost'] ?? $data['cost'],
        ];
        if(Arr::has($variant, 'quantity') || Arr::has($data, 'quantity')){
            $data['quantity'] = $variant['quantity'] ?? $data['quantity'];
        }

        $productVariant = $product->children()->create([
            'type' => 'product-variant',
            'sku' => $data['sku'],
            'slug' => $product->slug,
            'category_id' => $product->category_id,
        ]);

        $attributes_ids = Arr::pluck($variant['attributes'], 'id');
        $productVariant->options()->attach($attributes_ids);
        $this->productFlat->createProductFlat($data, $productVariant);
    }
    private function uploadImages($productFlat, $product)
    {
        if (request()->has('images')) {
            foreach (request('images') as $key => $image) {
                // Get filename with the extension
                $filenameWithExt = $image->getClientOriginalName();
                // Get just filename
                $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
                // Get just ext
                $extension = $image->getClientOriginalExtension();
                if($key == 'thumb'){

                    $fileNameToStore= 'product-' . $product->id .'.'.$extension;
                    $path = $image->storeAs('public/products/' . $product->id,  $fileNameToStore);
                    $fileNameToStore = Str::replaceFirst('public/', '', $path);
                    $productImage = $productFlat->update([
                        'thumbnail' => $fileNameToStore
                    ]);

                }else{
                    // Filename to store
                    $fileNameToStore= $filename.'.'.$extension;
                    // Upload Image
                    $path = $image->store('public/products/' . $product->id);

                    $fileNameToStore = Str::replaceFirst('public/', '', $path);

                    $productImage = $product->images()->create([
                        'path' => $fileNameToStore
                    ]);
                    $img = Image::make(public_path('storage/'.$productImage->path))->fit(130, 150, null, 'center');
                    $img->save();
                }

            }
        }
        return $productFlat;
    }
}
